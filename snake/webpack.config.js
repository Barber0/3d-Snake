module.exports = {
	entry: './src/app.ts',
	output: {
		filename: 'app.js',
		path: __dirname+"/built"
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		loaders: [
			{ test: /\.ts$/, loader: 'ts-loader' }
		]
	}     
}